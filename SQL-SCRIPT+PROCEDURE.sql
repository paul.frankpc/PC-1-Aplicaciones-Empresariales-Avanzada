create proc usp_itemList
as
select art_cod, art_nom, art_uni, art_pre, art_stk
from Articulos
go
create proc usp_sellerListByYear
@year varchar(4)
as 
select Ven_cod, Ven_nom, Ven_Fing
from Vendedor
where YEAR(Ven_Fing) = @year
order by Ven_Fing

select Ven_cod, Ven_nom, Ven_Fing,YEAR(Ven_Fing)
from Vendedor
order by Ven_Fing

go
create proc usp_clientListByName
@nombre varchar(50)
as
select cli_cod, cli_nom, cli_tel, cli_cor, cli_dir, cli_cre
from Clientes
where cli_nom LIKE '%'+@nombre+'%'
order by cli_nom
go
create proc usp_insertItem
@code char(5),
@nombre varchar(50),
@unidad varchar(4),
@precio numeric(8,2),
@stock int
as
INSERT Articulos(art_cod, art_nom, art_uni, art_pre, art_stk) VALUES (@code,@nombre,@unidad,@precio,@stock)
go
create proc usp_itemListNews
as
SELECT art_cod, art_nom, art_uni, art_pre, art_stk
from Articulos
order by art_cod desc

--select * from Articulos
--select * from Vendedor
--select * from Clientes