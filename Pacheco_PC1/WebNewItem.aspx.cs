﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pacheco_PC1
{
    public partial class WebNewItem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string code = txtCode.Text;
            string name = TxtNombre.Text;
            string unit = TxtUnit.Text;
            double price = Convert.ToDouble(TxtPrice.Text);
            int stock = Convert.ToInt32(TxtStock.Text);
            Rules pro = new Rules();
            pro.InsertItem(code, name, unit, price, stock);
            GridNewItems.DataSource = pro.newItemsList();
            GridNewItems.DataBind();
        }
    }
}