﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pacheco_PC1
{
    public partial class FormSellerByYear : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //enviar fecha a sql 
            string year = txtYear.Text;
            Rules pro = new Rules();
            GridSeller.DataSource = pro.SellerByDate(year);
            GridSeller.DataBind();
        }
    }
}