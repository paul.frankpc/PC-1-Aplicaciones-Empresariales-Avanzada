﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormItems.aspx.cs" Inherits="Pacheco_PC1.FormItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <h1>Lista de productos</h1>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GvItems" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="art_cod" HeaderText="codigo" />
                    <asp:BoundField DataField="art_nom" HeaderText="nombre" />
                    <asp:BoundField DataField="art_uni" HeaderText="unidad" />
                    <asp:BoundField DataField="art_pre" HeaderText="precio" />
                    <asp:BoundField DataField="art_stk" HeaderText="stock" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
