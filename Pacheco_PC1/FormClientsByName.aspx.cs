﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pacheco_PC1
{
    public partial class FormClientsByName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TxtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        protected void BuscaClientes_Click(object sender, EventArgs e)
        {
            Rules pro = new Rules();
            var nombre = TxtCliente.Text;
            GridClientes.DataSource = pro.ClientListByName(nombre);
            GridClientes.DataBind();
        }
    }
}