﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormSellerByYear.aspx.cs" Inherits="Pacheco_PC1.FormSellerByYear" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <h1>Busqueda de vendedores por Año</h1>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtYear" runat="server"></asp:TextBox>
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Buscar</asp:LinkButton>
        </div>
        <asp:GridView ID="GridSeller" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Ven_cod" HeaderText="codigo" />
                <asp:BoundField DataField="Ven_nom" HeaderText="Nombre de vendedor" />
                <asp:BoundField DataField="Ven_Fing" HeaderText="Fecha de Ingreso" />
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
