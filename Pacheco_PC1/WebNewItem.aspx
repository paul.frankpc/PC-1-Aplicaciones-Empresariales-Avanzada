﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebNewItem.aspx.cs" Inherits="Pacheco_PC1.WebNewItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Ingrese su nuevo Producto</h1>
            <table class="auto-style1">
                <tr>
                    <td>Codigo</td>
                    <td>Nombre</td>
                    <td>Unidad</td>
                    <td>Precio</td>
                    <td>Stock</td>
                </tr>
                <tr>
                    <td>
            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:TextBox ID="TxtNombre" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:TextBox ID="TxtUnit" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:TextBox ID="TxtPrice" runat="server"></asp:TextBox>
                    </td>
                    <td>
            <asp:TextBox ID="TxtStock" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Registrar producto</asp:LinkButton>
            <asp:GridView ID="GridNewItems" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="art_cod" HeaderText="Codigo" />
                    <asp:BoundField DataField="art_nom" HeaderText="Nombre" />
                    <asp:BoundField DataField="art_uni" HeaderText="Unidades" />
                    <asp:BoundField DataField="art_pre" HeaderText="Precio" />
                    <asp:BoundField DataField="art_stk" HeaderText="stock" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
