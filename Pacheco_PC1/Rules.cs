﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Pacheco_PC1
{
    public class Rules
    {
        ItemData item = new ItemData();

        public DataTable ItemList()
        {
            return item.ItemList();
        }
        public void InsertItem(string code, string name, string unit, double price, int stock)
        {
            item.InsertItem(code, name, unit, price, stock);
        }
        public DataTable ClientListByName(string name)
        {
            return item.ClientByName(name);
        }
        public DataTable SellerByDate(string year)
        {
            return item.SellerByYear(year);
        }
        public DataTable newItemsList()
        {
            return item.NewItemList();
        }
    }
}