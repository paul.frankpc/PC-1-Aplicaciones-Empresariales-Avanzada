﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormClientsByName.aspx.cs" Inherits="Pacheco_PC1.FormClientsByName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <h1>Busqueda de Clientes por Nombre</h1>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="TxtCliente" runat="server" OnTextChanged="TxtCliente_TextChanged"></asp:TextBox>
            <asp:LinkButton ID="BuscaClientes" runat="server" OnClick="BuscaClientes_Click">Buscar</asp:LinkButton>
            <asp:GridView ID="GridClientes" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="cli_cod" HeaderText="codigo" />
                    <asp:BoundField DataField="cli_nom" HeaderText="Nombre de Cliente" />
                    <asp:BoundField DataField="cli_tel" HeaderText="Telefono" />
                    <asp:BoundField DataField="cli_cor" HeaderText="Correo" />
                    <asp:BoundField DataField="cli_dir" HeaderText="Dirección" />
                    <asp:BoundField DataField="cli_cre" HeaderText="Credito" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
