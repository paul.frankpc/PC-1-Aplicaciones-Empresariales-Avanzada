﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pacheco_PC1
{
    public partial class FormItems : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Rules pro = new Rules();
            GvItems.DataSource = pro.ItemList();
            GvItems.DataBind();
        }
    }
}