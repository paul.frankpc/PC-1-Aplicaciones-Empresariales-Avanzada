﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MasterPage.aspx.cs" Inherits="Pacheco_PC1.MasterPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <h1>WilsonTECH - productos por mayor y menor</h1>
    <form id="form1" runat="server">
        <div>
            <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/FormItems.aspx">Lista de Articulos </asp:LinkButton>
            <br />
            <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/FormSellerByYear.aspx">Lista de vendedores por año</asp:LinkButton>
            <br />
            <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/FormClientsByName.aspx">lista de clientes por nombre</asp:LinkButton>
            <br />
            <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="~/WebNewItem.aspx">ingreso de productos</asp:LinkButton>
        </div>
    </form>
</body>
</html>
