﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Pacheco_PC1
{
    public class ItemData
    {
        Connection con = new Connection();

        public DataTable ItemList()
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter("usp_itemList", con.Cadena()))
            {
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                using (DataTable dataTable = new DataTable())
                {
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }

        public void InsertItem(string code, string name, string unit, double price, int stock)
        {
            SqlCommand cmd = new SqlCommand("usp_insertItem", con.Cadena());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@code", code);
            cmd.Parameters.AddWithValue("@nombre", name);
            cmd.Parameters.AddWithValue("@unidad", unit);
            cmd.Parameters.AddWithValue("@precio", price);
            cmd.Parameters.AddWithValue("@stock", stock);
     
            con.InsertBD(cmd);

        }

        public DataTable SellerByYear(string year)
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter("usp_sellerListByYear", con.Cadena()))
            {
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@year", year);
                using (DataTable dataTable = new DataTable())
                {
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }

        public DataTable ClientByName(string name)
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter("usp_clientListByName", con.Cadena()))
            {
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@nombre", name);
                using (DataTable dataTable = new DataTable())
                {
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }

        public DataTable NewItemList()
        {
            using (SqlDataAdapter adapter = new SqlDataAdapter("usp_itemListNews", con.Cadena()))
            {
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                using (DataTable dataTable = new DataTable())
                {
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }
    }
}